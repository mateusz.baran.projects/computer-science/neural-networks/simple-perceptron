import numpy as np


class SLNN:
    def __init__(self, input_shape, learning_rate=0.01, activation='unipolar',
                 quantization='identity', theta_activation=0.5, theta_quantization=0.5,
                 use_bias=False, low=-1, high=1, verbose=True):
        self.W = np.random.uniform(low, high, size=(input_shape[1], 1))
        self.b = np.zeros((1, 1))
        self.learning_rate = learning_rate
        self.activation = activation
        self.quantization = quantization
        self.theta_activation = theta_activation
        self.theta_quantization = theta_quantization
        self.use_bias = use_bias
        self.verbose = verbose

    def _activate(self, z):
        if self.activation == 'bipolar':
            return np.where(z > self.theta_activation, 1, -1)
        if self.activation == 'unipolar':
            return np.where(z > self.theta_activation, 1, 0)
        return z

    def _quantize(self, y_pred):
        if self.quantization == 'bipolar':
            return np.where(y_pred > self.theta_quantization, 1, -1)
        if self.quantization == 'unipolar':
            return np.where(y_pred > self.theta_quantization, 1, 0)
        return y_pred

    def _feed(self, x):
        return self._activate(x @ self.W + self.b)

    def _back(self, x, error):
        self.W = self.W + self.learning_rate * x.T @ error
        if self.use_bias:
            self.b = self.b + self.learning_rate * np.sum(error, axis=0, keepdims=True)

    def train(self, x, y, epochs=1000000, eta=0):
        for epoch in range(epochs):
            y_pred = self._feed(x)
            error = y - y_pred
            loss = np.mean(error ** 2)

            if self.verbose:
                print('loss', loss)
            if loss <= eta:
                break

            self._back(x, error)

        return epoch, loss, self._quantize(y_pred)


if __name__ == '__main__':
    X = np.array([
        [0, 0],
        [0, 1],
        [1, 0],
        [1, 1],
    ], dtype=np.float16)

    Y = np.array([
        [0],
        [1],
        [1],
        [1],
    ], dtype=np.float16)

    model = SLNN(X.shape, learning_rate=0.01, activation='unipolar',
                 theta_activation=0, use_bias=True)
    iterations, loss, output = model.train(X, Y, eta=0)

    print('Total epochs', iterations)
    print('Prediction', output)
    print('Weights', model.W)
    print('Bias', model.b)
