import csv
import numpy as np
from single_layer_neural_network import SLNN

X = np.array([
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1],
], dtype=np.float16)

Y_or = np.array([
    [0],
    [1],
    [1],
    [1],
], dtype=np.float16)

Y_and = np.array([
    [0],
    [0],
    [0],
    [1],
], dtype=np.float16)

with open('results.csv', mode='w') as file:
    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['dataset', 'learning_rate', 'fn_activation', 'fn_quantization', 'use_bias',
                     'theta_activation', 'theta_quantization', 'low', 'high', 'eta', 'seed', 'loss',
                     'prediction', 'epochs'])
    for name, Y in [('OR', Y_or), ('AND', Y_and)]:
        models = [
            ('unipolar', .5, 'identity', 0, False, Y, 0),
            ('bipolar', .5, 'identity', 0, False, (2 * Y - 1), 0),
            ('identity', 0, 'unipolar', 0, True, Y, 0.07),
            ('identity', 0, 'bipolar', 0, True, (2 * Y - 1), 0.26),
        ]
        for act, theta_act, qua, theta_qua, use_bias, y, eta in models:
            for lr in [0.5, 0.1, 0.05, 0.01, 0.005, 0.001]:
                for low, high in [(-1, 1), (-.8, .8), (-.5, .5), (-.2, .2), (-.05, .05), (-.0, .0)]:
                    for seed in range(1, 11):
                        np.random.seed(seed)
                        model = SLNN(X.shape, learning_rate=lr, activation=act,
                                     theta_activation=theta_act, quantization=qua,
                                     theta_quantization=theta_qua, low=low, high=high,
                                     use_bias=use_bias, verbose=False)
                        epoch, loss, output = model.train(X, y, epochs=1000, eta=eta)
                        pred = np.all(y == output)
                        print(f'{name}, act: {act}, qua: {qua}, lr: {lr}, range: ({low}, {high}) - '
                              f'loss: {loss} - prediction: {pred} - epochs: {epoch + 1}')
                        writer.writerow([name, lr, act, qua, use_bias, theta_act,
                                         theta_qua, low, high, eta, seed, loss, pred, epoch])
