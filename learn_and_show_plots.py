import numpy as np
import matplotlib.pyplot as plt
from single_layer_neural_network import SLNN

X = np.array([
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1],
], dtype=np.float16)

Y = np.array([
    [0],
    [0],
    [0],
    [1],
], dtype=np.float16)

perceptron_dict = {
    'input_shape': X.shape,
    'learning_rate': 0.01,
    'activation': 'unipolar',
    'theta_activation': .5,
    'quantization': 'identity',
    'theta_quantization': 0,
    'low': -.2,
    'high': .2,
    'use_bias': False,
    'verbose': False,
}

adaline_dict = {
    'input_shape': X.shape,
    'learning_rate': 0.01,
    'activation': 'identity',
    'theta_activation': 0,
    'quantization': 'bipolar',
    'theta_quantization': 0,
    'low': -.2,
    'high': .2,
    'use_bias': True,
    'verbose': False,
}


def epochs_of_init_weight(model_dict, y, eta):
    xx = []
    yy = []
    y_err = []
    for low, high in [(-1, 1), (-.8, .8), (-.5, .5), (-.2, .2), (-.1, .1), (-.05, .05), (-.0, .0)]:
        model_dict['low'] = low
        model_dict['high'] = high

        epochs, error, correct_predictions = learn(model_dict, X, y, eta)
        print('Number of correct prediction:', correct_predictions)
        if correct_predictions:
            xx.append(str(f'({low},{high})'))
            yy.append(epochs)
            y_err.append(error)

    plot([[xx, yy, y_err]], 'Zakres inicjacji wag', 'Liczba epok',
         'Szybkosc uczenia wzaleznosci od zakresu inicjacji wag')


def epochs_of_learning_rate(model_dict, y, eta):
    xx = []
    yy = []
    yy2 = []
    y_err = []
    for learning_rate in [0.5, 0.1, 0.05, 0.01, 0.005, 0.001]:
        model_dict['learning_rate'] = learning_rate

        epochs, error, correct_predictions = learn(model_dict, X, y, eta)
        print('Number of correct prediction:', correct_predictions)
        if correct_predictions:
            xx.append(learning_rate)
            yy.append(epochs)
            yy2.append(correct_predictions)
            y_err.append(error)

    plot([[xx, yy, y_err]], 'Współczynnik uczenia', 'Liczba epok',
         'Szybkosc uczenia wzaleznosci od wspolczynnika uczenia')
    plot([[xx, yy2, None]], 'Współczynnik uczenia', 'Liczba poprawnych predykcji [%]',
         'Liczba poprawnych predykcji wzaleznosci od wspolczynnika uczenia')


def epochs_of_fn_activate(model_dict):
    xx = []
    yy = []
    y_err = []
    for y, activation in [(Y, 'unipolar'), ((2 * Y - 1), 'bipolar')]:
        model_dict['activation'] = activation

        epochs, error, correct_predictions = learn(model_dict, X, y, 0)
        print('Number of correct prediction:', correct_predictions)
        if correct_predictions:
            xx.append(activation)
            yy.append(epochs)
            y_err.append(error)

    plot([[xx, yy, y_err]], 'Funkcja aktywacji', 'Liczba epok',
         'Szybkosc uczenia wzaleznosci od funkcji aktywacji')


def epochs_of_init_weight_perceptron_vs_adaline():
    data = []
    for name, y, eta, model_dict in [('Perceptron', Y, 0, perceptron_dict),
                                     ('AdaLine', (2 * Y - 1), .3, adaline_dict)]:
        xx = []
        yy = []
        y_err = []
        for low, high in [(-5, 5), (-3, 3), (-1, 1), (-.5, .5), (-.3, .3), (-.1, .1), (-.0, .0)]:
            model_dict['low'] = low
            model_dict['high'] = high

            epochs, error, correct_predictions = learn(model_dict, X, y, eta)
            print('Number of correct prediction:', correct_predictions)
            if correct_predictions:
                xx.append(str(f'({low},{high})'))
                yy.append(epochs)
                y_err.append(error)

        data.append([xx, yy, y_err, name])

    plot(data, 'Zakres inicjacji wag', 'Liczba epok',
         'Szybkosc uczenia wzaleznosci od algorytmu i zakresu inicjacji wag')


def learn(model_dict, x, y, eta, nb_epochs=1000, iterations=100):
    epochs = []
    correct_predictions = 0
    for seed in range(iterations):
        np.random.seed(seed)
        model = SLNN(**model_dict)
        epoch, loss, output = model.train(x, y, epochs=nb_epochs, eta=eta)

        if np.all(y == output):
            epochs.append(epoch)
            correct_predictions += 1

    if epochs:
        mean = np.mean(epochs)
        return np.mean(epochs), [mean - np.min(epochs), np.max(epochs) - mean], correct_predictions
    else:
        return 0., [0., 0.], 0


def plot(data, xlabel, ylabel, title=''):
    fig, ax = plt.subplots(figsize=(9, 5))
    fig.canvas.set_window_title(title)
    ax.grid(True)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    for xx, yy, y_err, *legend in data:
        y_err = np.transpose(y_err)
        legend = legend[0] if len(legend) else None
        ax.errorbar(xx, yy, yerr=y_err, fmt='o', label=legend)

    ax.legend()


epochs_of_init_weight(perceptron_dict, Y, 0)
epochs_of_learning_rate(perceptron_dict, Y, 0)
epochs_of_fn_activate(perceptron_dict)

epochs_of_init_weight(adaline_dict, (2 * Y - 1), 0.3)
epochs_of_learning_rate(adaline_dict, (2 * Y - 1), 0.3)
epochs_of_init_weight_perceptron_vs_adaline()

plt.show()
